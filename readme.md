#2016-03-14

HTML Files in this folder are public and can be accessed via anteante.bitbucket.org

To add a subfolder / project, clone the repository, add a subfolder, e.g.

`clientproject`

and push to the remote repro.

To un-publish a client project, remove the repro locally

`rm -r clientproject`

and push to the remote repro again.
